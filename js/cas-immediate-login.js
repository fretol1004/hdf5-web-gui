/*global $*/
'use strict';

// The gloabl variables for this applicaiton
var SERVER_COMMUNICATION, Ping, CONFIG_DATA,

    // The gloabl variables for this applicaiton
    CAS_IMMEDIATE = {

        casServer : '',

        // Log into the CAS server
        loginCAS : function () {

            var debug = true, redirectUrl, htmlLoadMsg = '', htmlErrMsg = '';

            document.getElementById("loadMsg").innerHTML =
                'Attempting to contact HDF5 server... ';

            // From the h5 server, get the CAS server configuration
            $.when(CAS_IMMEDIATE.getCASConfig()).then(function (response) {

                if (debug) {
                    console.log('HDF5 server reached ');
                    console.log(response);
                }

                htmlLoadMsg += '<span style="color:green">success</span></br>';
                htmlLoadMsg += 'Checking if authentication is required... ';

                CAS_IMMEDIATE.casServer = response.casServer;

                if (debug) {
                    console.log('cas server:  ' + CAS_IMMEDIATE.casServer);
                }

                // Check if the CAS url given is valid
                if (CAS_IMMEDIATE.isValidURL(CAS_IMMEDIATE.casServer)) {

                    htmlLoadMsg += '<span style="color:blue">yes</span>';
                    htmlLoadMsg += '</br>Attempting to contact ';
                    htmlLoadMsg += 'authentication server... ';

                    // Construct the login url which contains the service url
                    // to which the browser will be redirected after
                    // successfully logging into CAS
                    // redirectUrl = CAS_IMMEDIATE.casServer +
                    //     '/cas/login?service=' +
                    //     encodeURIComponent(CONFIG_DATA.serviceUrl);

                    // redirectUrl = CONFIG_DATA.hdf5DataServer + '/login';
                    redirectUrl = CAS_IMMEDIATE.casServer +
                        '/cas/login?service=' +
                        encodeURIComponent(CONFIG_DATA.serviceUrl);

                    if (debug) {
                        console.debug('Redirecting to ' + redirectUrl);
                    }


                } else {

                    console.log('Invalid CAS url given, assumming no CAS' +
                        ' authentication is required');

                    // Url to redirect to the app page, skipping CAS
                    redirectUrl = CONFIG_DATA.serviceUrl;

                    htmlLoadMsg += '<span style="color:blue">no</span>';
                }

                htmlErrMsg += 'Redirecting to ' + redirectUrl;

                if (debug) {
                    console.log('Redirecting to ' + redirectUrl);
                }

                // Place some info on html page
                document.getElementById("loadMsg").innerHTML += htmlLoadMsg;
                document.getElementById("errMsg").innerHTML += htmlErrMsg;

                // Redirect to either the CAS server or directly to the app
                window.location = redirectUrl;

            });

        },


        // Check with the HDF5 server to see if CAS use is configured
        getCASConfig : function () {

            var debug = true, casCheckUrl =
                CONFIG_DATA.hdf5DataServer + '/usecas/';

            if (debug) {
                console.log('casCheckUrl: ' + casCheckUrl);
            }

            return SERVER_COMMUNICATION.ajaxRequest(casCheckUrl, debug);
        },


        // Check if a given string is a valid url
        isValidURL : function (str) {

            var debug = false, parser = document.createElement('a');
            parser.href = str;

            if (str === 'None') {
                str = '';
            }

            if (debug) {
                console.log(str);
            }

            return (parser.host && parser.host !== window.location.host);
        },

    };

CAS_IMMEDIATE.loginCAS();
